package server;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server extends CarUtil {

	private static ICarUtil stub = null;
	public Server(int port) throws IOException, AlreadyBoundException {
		CarUtil carUtil = new CarUtil();
		stub = (ICarUtil) UnicastRemoteObject.exportObject(carUtil, port);
		Registry registry = LocateRegistry.createRegistry(port);
		registry.bind("ICarUtil", stub);
	}

	public static void main(String[] args) {
		try {
			new Server(8889);
			System.out.println("The server started.");
		} catch (IOException e) {
			//LOGGER.error("",e);
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}
	}

}