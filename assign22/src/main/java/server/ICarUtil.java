package server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import model.Car;

public interface ICarUtil extends Remote {
    double getTax(Car car) throws RemoteException;
    double getPrice(Car car) throws RemoteException;
}
