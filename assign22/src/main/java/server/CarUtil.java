package server;

import model.Car;
public class CarUtil implements ICarUtil {

    public double getPrice(Car c) {
        if (2018 - c.getYear() >= 7) {
            return 0;
        } else {
            double value = c.getPurchasingPrice() - ((double)c.getPurchasingPrice() / 7) * (2018 - c.getYear());
            return value;
        }
    }

    public double getTax(Car c) {
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineCapacity() > 1601) sum = 18;
        if(c.getEngineCapacity() > 2001) sum = 72;
        if(c.getEngineCapacity() > 2601) sum = 144;
        if(c.getEngineCapacity() > 3001) sum = 290;
        return c.getEngineCapacity() / 200.0 * sum;
    }
}
