package client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Car;
import server.ICarUtil;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Presentation extends Application {
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Car calculator");
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) {

            }
        });
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        Label labelPrice = new Label("Price");
        grid.add(labelPrice, 0, 0);
        final TextField textFieldPrice = new TextField();
        grid.add(textFieldPrice, 1, 0);

        Label labelYear = new Label("Year");
        grid.add(labelYear, 0, 1);
        final TextField textFieldYear = new TextField();
        grid.add(textFieldYear, 1, 1);

        Label labelEngine = new Label("Engine");
        grid.add(labelEngine, 0, 2);
        final TextField textFieldEngine = new TextField();
        grid.add(textFieldEngine, 1, 2);
        final Label labelDisplayTax = new Label("");
        grid.add(labelDisplayTax, 0, 3);
        final Label labelDisplayPrice = new Label("");
        grid.add(labelDisplayPrice, 1, 3);
        Button button1 = new Button("Calculate price");
        Button button2 = new Button("Calculate tax");

        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                //label.setText("Accepted");
                Registry registry = null;
                try {
                    registry = LocateRegistry.getRegistry(8889);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
                ICarUtil iCarUtil = null;
                try {
                    iCarUtil = (ICarUtil) registry.lookup("ICarUtil");
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                } catch (NotBoundException e1) {
                    e1.printStackTrace();
                }
                try {
                    double  price = iCarUtil.getPrice(new Car(Integer.parseInt(textFieldYear.getText()), Integer.parseInt(textFieldEngine.getText()), Integer.parseInt(textFieldPrice.getText())));
                    labelDisplayTax.setText("");
                    labelDisplayPrice.setText("PRETUL ESTE  " + price);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        });

        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                //label.setText("Accepted");
                Registry registry = null;
                try {
                    registry = LocateRegistry.getRegistry(8889);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
                ICarUtil iCarUtil = null;
                try {
                    iCarUtil = (ICarUtil) registry.lookup("ICarUtil");
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                } catch (NotBoundException e1) {
                    e1.printStackTrace();
                }
                try {
                    double  tax = iCarUtil.getTax(new Car(Integer.parseInt(textFieldYear.getText()), Integer.parseInt(textFieldEngine.getText()), Integer.parseInt(textFieldPrice.getText())));
                    //System.out.println("TAXA ESTE" + tax);
                    labelDisplayPrice.setText("");
                    labelDisplayTax.setText("TAXA ESTE" + tax);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        });
        grid.add(button1, 0, 4);
        grid.add(button2, 1, 4);

        Scene scene = new Scene(grid, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
